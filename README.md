# Plan du projet

## Pages

- Charte natale (Formulaire)
- Astrologie du jour
    - liste de description courte d'Astrologie du jour de chaque signe astrologique
    - images
- Page d'accueil
    - Brève présentation de ce qu'est "Astrologie Orion"
    - Information générale sur l'astrologie ainsi qu'un site "référence" organisé grâce à un Menu
- Mon horoscope 
    - Formulaire

    
## Composantes présente à chaque pages
- Navbar 
    - Logo
    - Liens de navigation pages principales du site
- Footer
    - liens utiles
    - "Copyright"
    - Date de mise à jour
    - Nous contacter (email, phone)
    grid => 1fr 1fr
    <hr>
    1fr 1fr 1fr;

    OR flex-direction; row.



# examples
https://demo.templatemonster.com/fr/demo/276523.html